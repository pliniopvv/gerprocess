/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GerProcess;

import GerProcess.obj.RootView;
import GerProcess.obj.Root;
import GerProcess.obj.RootFile;
import GerProcess.obj.TarefaView;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Plinio
 */
public class init {

    //LOOK AND FEEL - INÍCIO
    public static void main(String[] args) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows Classic".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(TarefaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //LOOK AND FEEL - FIM


        try {
            Root r = RootFile.load();
            RootView rv = new RootView(r);
            rv.start();

        } catch (FileNotFoundException ex) {
            System.out.println("O Arquivo pode estar corrompido!");
        } catch (IOException ex) {
            System.out.println("Falha em ler o arquivo!");
        }


//        try {
//            HD.saveFileObject(f, r);
//        } catch (IOException e) {
//            alert("Falha em salvar o arquivo Root:" + e.getMessage());
//        }
    }

    public static void alert(String msg) {
        JOptionPane.showMessageDialog(null, msg, "ERRO!", JOptionPane.ERROR_MESSAGE);
    }
}