/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GerProcess.obj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author PLINIO
 */
public class Processo extends ArrayList<Tarefa> implements Comparable<Processo> {

    private String titulo = "";

    public Processo() {
    }

    public Processo(String titulo) {
        this.titulo = titulo;
    }

    public static Processo buildProcessWithView() {
        Processo p = new Processo();
        ProcessoView pv = new ProcessoView(p);
        pv.start();
        return pv.getProcesso();
    }

    public void addTarefa(Tarefa tarefa) {
        add(tarefa);
    }

    public Processo(String titulo, Tarefa[] tarefas) {
        this.titulo = titulo;
        addAll(Arrays.asList(tarefas));
    }

    @Deprecated
    public int count() {
        return size();
    }

    public int CountNotFinished() {
        int v = size();
        int c = 0;
        for (Tarefa t : toArray()) {
            if (!t.getStatus()) {
                c++;
            }
        }
        return c;
    }

    public Tarefa[] getTarefaNFinalizada() {
        Tarefa[] tall = toArray();
        ArrayList<Tarefa> tnf = new ArrayList<>();
        for (Tarefa t : tall) {
            if (!t.getStatus()) {
                tnf.add(t);
            }
        }
        return tnf.toArray(new Tarefa[tnf.size()]);
    }

    public static Processo buildProcessoWithView() {
        Processo p = new Processo();
        ProcessoView pv = new ProcessoView(p);
        pv.start();
        return p;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public Tarefa[] toArray() {
        return toArray(new Tarefa[size()]);
    }

    public Date getNextDate() {
        int cNF = CountNotFinished();
        if (cNF >= 2) {
            Tarefa[] tall = getTarefaNFinalizada();
            Date sData = tall[0].getData();
            for (Tarefa t : tall) {
                Date d = t.getData();
                if (sData.after(d)) {
                    sData = d;
                }
            }
            return sData;
        } else if (cNF == 1) {
            return getTarefaNFinalizada()[0].getData();
        } else {
            return new Date("11/11/1111");
        }
//        Date sData = get(0).getData();
//        for (int i = 1; i < size(); i++) {
////            String tit = get(i - 1).getTitulo();
////            String dd1 = get(i - 1).getDataToString();
////            String dd2 = get(i).getDataToString();
//            Tarefa t1 = get(i - 1);
//            Tarefa t2 = get(i);
//            if (!t2.getStatus()) {
//                Date d2 = get(i).getData();
//                if (sData.after(d2)) {
//                    sData = d2;
//                } else if (sData.equals(d2)) {
//                    sData = d2;
//                }
//            }
    }

    public String getNextDateToString() {
        Date sData = getNextDate();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(sData);
    }

    @Override
    public int compareTo(Processo p2) {
        Date d1 = getNextDate();
        Date d2 = p2.getNextDate();

        if (d1.after(d2)) {
            return 1;
        } else if (d1.before(d2)) {
            return -1;
        } else {
            return 0;
        }
    }
}
