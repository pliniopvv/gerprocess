/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GerProcess.obj.cmp;

import GerProcess.obj.Root;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author PLINIO
 */
public class ProcessoTableModel extends AbstractTableModel {

    private Root gp;
    //
    public static final int COL_NUM = 0;
    public static final int COL_DATA = 1;
    public static final int COL_TITULO = 2;

    public ProcessoTableModel(Root gp) {
        this.gp = gp;
    }

    @Override
    public int getRowCount() {
        return gp.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int col) {
        switch (col) {
            case COL_NUM:
                return "Nº";
            case COL_DATA:
                return "Data";
            case COL_TITULO:
                return "Processo";
            default:
                return "N/A";
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch (col) {
            case COL_NUM:
                return row + 1;
            case COL_DATA:
                return gp.get(row).getNextDateToString();
            case COL_TITULO:
                return gp.get(row).getTitulo();
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return false;
    }
}
