/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GerProcess.obj.cmp;

import GerProcess.obj.Processo;
import GerProcess.obj.Tarefa;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Plinio
 */
public class TarefaTableModel extends AbstractTableModel {

    private Processo processo;

    public TarefaTableModel(Processo memoria) {
        this.processo = memoria;
    }

    @Override
    public int getRowCount() {
        return processo.size();
    }
    public static final int COL_NUM = 0;
    public static final int COL_DATA = 1;
    public static final int COL_TITULO = 2;
    public static final int COL_STATUS = 3;

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int col) {
        switch (col) {
            case COL_NUM:
                return "Nº";
            case COL_DATA:
                return "DATA";
            case COL_TITULO:
                return "Titulo";
            case COL_STATUS:
                return "Status";
        }
        return null;
    }

    public Tarefa getTarefa(int row) {
        return processo.get(row);
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
//        System.out.print(o + " ");
//        System.out.print(row + " ");
//        System.out.println(col);
        processo.get(row).setStatus((boolean) o);
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch (col) {
            case COL_NUM:
                return (row + 1);
            case COL_DATA:
                return processo.get(row).getDataToString();
            case COL_TITULO:
                return processo.get(row).getTitulo();
            case COL_STATUS:
                return processo.get(row).getStatus();
            default:
                JOptionPane.showMessageDialog(null, "ERRO!", "Ocorreu um erro na TarefaTableModel, favor avisar o Desenvolvedor!", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if (col == COL_STATUS) {
            return Boolean.class;
        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == COL_STATUS) {
            return true;
        }
        return false;
    }
}
