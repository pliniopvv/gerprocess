/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GerProcess.obj;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author PLINIO
 */
public class Tarefa implements Serializable, Comparable<Tarefa> {

    //PÚBLICOS
    public static final Boolean CONCLUIDO = true;
    public static final Boolean NAO_CONCLUIDO = false;
    //PARAMETROS
    private String Titulo;
    private String Descricao;
    private Boolean status;
    private Date data;

    public static Tarefa buildTarefaWithView() {
        Tarefa t = new Tarefa();
        TarefaView tv = new TarefaView(t);
        tv.start();
        return tv.getTarefa();
    }

    public Tarefa() {
        this.Titulo = "";
        this.Descricao = "";
        this.status = NAO_CONCLUIDO;
        this.data = new Date();
    }

    public Tarefa(String Titulo, String Descricao, Boolean status, Date data) {
        this.Titulo = Titulo;
        this.Descricao = Descricao;
        this.status = status;
        this.data = data;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getData() {
        return data;
    }

    public String getDataToString() {
        SimpleDateFormat sdf = new SimpleDateFormat("d/M/y");
        return sdf.format(data);
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public int compareTo(Tarefa t) {
        Date d1 = getData();
        Date d2 = t.getData();
        if (d1.after(d2)) {
            return 1;
        } else if (d1.before(d2)) {
            return -1;
        } else {
            return 0;
        }
    }
}
