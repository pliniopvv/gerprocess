/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GerProcess.obj;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author PLINIO
 */
public class Root extends ArrayList<Processo> {

    public Root() {
    }

    public Root(Processo[] processo) {
        addAll(Arrays.asList(processo));
    }

    @Override
    public Processo[] toArray() {
        return toArray(new Processo[size()]);
    }
}