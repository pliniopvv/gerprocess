/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GerProcess.obj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author PLINIO
 */
public abstract class RootFile {

    private static String DATA_ROOT = "Data/Dados.GProcess";
    //
    private static String R_ESPACO = new String("" + ((char) 219));
    private static String R_ENTER = new String("" + ((char) 186));
//    private static String R_PONTO_E_VIRGULA = new String("" + ((char) 191));

    public static void save(Root root) throws FileNotFoundException {
        File f = new File("Data/Dados.GProcess");
        if (!f.exists()) {
            File d = new File("Data");
            if (!d.exists()) {
                d.mkdir();
            }
        }
        PrintWriter pw = new PrintWriter(f);
        for (int ip = 0; ip < root.size(); ip++) {
            String processoTitulo = "";
            Processo p = root.get(ip);
            processoTitulo = p.getTitulo();
            for (int it = 0; it < p.size(); it++) {
                Tarefa t = p.get(it);
                String tarefaTitulo = t.getTitulo();
                String tarefaDescricao = t.getDescricao();
                String tarefaData = t.getDataToString();
                String tarefaStatus = String.valueOf(t.getStatus());
                StringBuffer sb = new StringBuffer();
                sb.append(processoTitulo
                        + ";" + tarefaTitulo
                        + ";" + tarefaDescricao
                        + ";" + tarefaData
                        + ";" + tarefaStatus);

                String data = sb.toString();
                data = valiData(data);
                pw.write(data + "\n");
            }
        }
        pw.flush();
        pw.close();
    }

    public static Root load() throws FileNotFoundException, IOException {
        Root root = new Root();
        File f = new File("Data/Dados.GProcess");
        if (!f.exists()) { // SE O ARQUIVO NÃO EXISTIR;
            File d = new File("Data");
            if (!d.exists()) {
                d.mkdir();
            }
            return new Root();
        }
        FileReader fr = new FileReader(f);
        BufferedReader bf = new BufferedReader(fr);
        String s = "";

        HashMap<String, Processo> p = new HashMap();
        HashSet ip = new HashSet();

        int PROCESSO_TITULO = 0;
        int TAREFA_TITULO = 1;
        int TAREFA_DESCRICAO = 2;
        int TAREFA_DATA = 3;
        int TAREFA_STATUS = 4;

        while ((s = bf.readLine()) != null) {
            String[] elem = s.split(";");

            for (int i = 0; i < elem.length; i++) { // DECODIFICANDO
                String l = elem[i];
                if (l.contains(R_ENTER)) {
                    elem[i] = elem[i].replaceAll(R_ENTER, "\n");
                }
                if (l.contains(R_ESPACO)) {
                    elem[i] = elem[i].replaceAll(R_ESPACO, " ");
                }
//                if (l.contains(R_PONTO_E_VIRGULA)) {
//                    elem[i] = elem[i].replaceAll(R_PONTO_E_VIRGULA, ";");
//                }
            }

            String[] d = elem[TAREFA_DATA].split("/");
            elem[TAREFA_DATA] = d[1] + "/" + d[0] + "/" + d[2];

            if (ip.add(elem[PROCESSO_TITULO])) {
                p.put(elem[PROCESSO_TITULO], new Processo(elem[PROCESSO_TITULO]));
            }

            ((Processo) p.get(elem[PROCESSO_TITULO]))
                    .add(new Tarefa(
                    elem[TAREFA_TITULO],
                    elem[TAREFA_DESCRICAO],
                    Boolean.valueOf(elem[TAREFA_STATUS]),
                    new Date(elem[TAREFA_DATA])));

        }
        for (Map.Entry<String, Processo> pEntry : p.entrySet()) {
            Processo pUnid = pEntry.getValue();
            root.add(pUnid);
        }
        return root;
    }

    private static String valiData(String data) {
        String data1 = data.replaceAll("\n", R_ENTER);
        String data2 = data1.replaceAll(" ", R_ESPACO);
//        String data3 = data1.replaceAll(";", R_PONTO_E_VIRGULA);
        return data2;
    }

    private static String DevaliData(String data) {
        if (data instanceof String) {
//            String data1 = data.replaceAll(R_PONTO_E_VIRGULA, ";");
            String data1 = data.replaceAll(R_ENTER, "\n");
            String data2 = data1.replaceAll(R_ESPACO, " ");
            return data2;
        } else {
            return null;
        }
    }
}
